import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import "./App.scss";
import "./Responsive.scss";
import Home from "./components/home";
import Navbar from "./components/navbar";
import Links from "./components/links";
import Summary from "./components/summary";
import Cluster from "./components/cluster";

const history = require("history").createBrowserHistory;

function App() {
  return (
    <div className="App">
      <section class="header-main">
        <div class="container">
          <div class="logo">
            <a href="https://covidtracker.online/">
              <img src={process.env.PUBLIC_URL + "assets/images/logo.png"} />
            </a>
          </div>
        </div>
      </section>
      <Router history={history}>
        <Route
          render={({ location }) => (
            <div className="Almighty-Router">
              <Route exact path="/" render={() => <Redirect to="/" />} />
              <Switch location={location}>
                <Route exact path="/" render={props => <Home {...props} />} />
                <Route
                  exact
                  path="/links"
                  render={props => <Links {...props} />}
                />
                <Route
                  exact
                  path="/summary"
                  render={props => <Summary {...props} />}
                />
                <Route
                  exact
                  path="/networkmap"
                  render={props => <Cluster {...props} />}
                />
              </Switch>
            </div>
          )}
        />
      </Router>

      <footer class="footer-area footer--light">
        <div class="mini-footer">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="copyright-text">
                  <p>
                    Source: WHO, CDC, ECDC, NHC, DXY, MOHFW, BNO News, The Hindu
                  </p>
                  <a target="_blank" href="mailto:lightseekers20@gmail.com">
                    <button type="button" class="btn btn-info contact-us">
                      Contact Us
                    </button>
                  </a>
                </div>

                <div class="go_top">
                  <span class="icon-arrow-up"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default App;
